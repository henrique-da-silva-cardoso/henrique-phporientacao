<?
Class Conta {
    protected $saldo = 0;
    protected $titular = 'Diego';

    function __construct()  {
        
    }

    function titular($titular) {
        $this -> titular = $titular;
    }
    
    function sacar($valor){
        if($this -> saldo > 0 && $this -> saldo >= $valor) {
            $_SESSION['CONTA BANCARIA'][$this -> titular]['saldo'] = $this -> saldo;
            $_SESSION['CONTA BANCARIA'][$this -> titular]['saque'] = $valor;
            
            $this -> saldo -= $valor;

            $_SESSION['CONTA BANCARIA'][$this -> titular]['novoSaldoAposSaque'] = $this -> saldo;
            
        } else {
            echo 'Saldo Insuficiente <br>';
        }
    }

    function depositar($valor) {
        $_SESSION['CONTA BANCARIA'][$this -> titular]['deposito'] = $valor;
        $this -> saldo += $valor;
        $_SESSION['CONTA BANCARIA'][$this -> titular]['novoSaldocomDeposito'] = $this -> saldo;
    }

    function verSaldo() {
        echo 'Saldo Atual: '.$this -> saldo.'<br>';
    }
}
?>