<?
Class Jogo {
    protected $nome;
    protected $genero;
    protected $criador;
    protected $exclusivo;
    protected $plataformas;
    protected $jogadores = 1;

    function __construct($nome, $genero, $criador, $plataformas)
    {
        $this -> nome = $nome;
        $this -> genero = $genero;
        $this -> criador = $criador;
        $this -> plataformas = $plataformas;
        
        $_SESSION['JOGO'][$this->nome]['nome'] = $this->nome;
        $_SESSION['JOGO'][$this->nome]['genero'] = $this->genero;
        $_SESSION['JOGO'][$this->nome]['criador'] = $this->criador;
        $_SESSION['JOGO'][$this->nome]['plataformas'] = $this->plataformas;
    }

    function exclusivo($exclusivo) {
        $this -> exclusivo = $exclusivo;
        $_SESSION['JOGO'][$this->nome]['exclusivo'] = $this->exclusivo;
    }

    function jogadores($jogadores) {
        $this -> jogadores = $jogadores;
        $_SESSION['JOGO'][$this->nome]['jogadores'] = $this->jogadores;
    }

    function verJogo() {
        echo 'O nome desse jogo é: '.$_SESSION['JOGO'][$this->nome]['nome'].'.<br>';
        echo 'O gênero desse jogo é: '.$_SESSION['JOGO'][$this->nome]['genero'].'.<br>';
        echo 'Esse jogo foi criado por: '.$_SESSION['JOGO'][$this->nome]['criador'].'.<br>';
        $textoPlataforma = 'Esse jogo pode ser jogado nas seguintes plataformas: ';
        foreach($_SESSION['JOGO'][$this->nome]['plataformas'] as $plataforma) {
            $textoPlataforma .= $plataforma.', ';
        };
        echo substr_replace($textoPlataforma,'',-2).'.<br>';

        if($this->exclusivo){
            echo 'Esse jogo é exclusivo da plataforma: '.$_SESSION['JOGO'][$this->nome]['exclusivo'].'<br>';
        } else {
            echo 'Esse jogo não é exclusivo a uma plataforma<br>';
        }

        echo 'Esse jogo pode ser jogado por '.($_SESSION['JOGO'][$this->nome]['jogadores'] == 1 ? $_SESSION['JOGO'][$this->nome]['jogadores'].' jogador' : $_SESSION['JOGO'][$this->nome]['jogadores'].' jogadores').'<br>';
        echo '<br>';
    }

}
?>